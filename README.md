<div align="center">
  <img src="https://gitlab.com/kkrishguptaa-learns/border-radius-previewer/-/raw/main/assets/logo.png" height="100px" width="100px" />
  <br />
  <h1>Border Radius Previewer</h1>
  <p>Minimalistic Border Radius Previewer</p>
  <p><a href="https://kkrishguptaa-learns.gitlab.io/border-radius-previewer"><img src="https://img.shields.io/badge/View%20Deployed-2965F1?style=for-the-badge" alt="View Deployed" /></a></p>
</div>

## 📸 Screenshots

| Desktop | Mobile |
| --- | --- |
| ![Screenshot of the application, the page includes a heading "Border Radius Previewer", a textbox for input border radiuses and the output as a square element with the border-radius style](https://gitlab.com/kkrishguptaa-learns/border-radius-previewer/-/raw/main/assets/screenshots/desktop.png) |   ![Screenshot of Border Radius Previewer On A Mobile Device To Showcase That It's Responsive](https://gitlab.com/kkrishguptaa-learns/border-radius-previewer/-/raw/main/assets/screenshots/mobile.png)  |

## 💡 Origin

Details can be found => [#3](https://github.com/kkrishguptaa/learning/issues/3)